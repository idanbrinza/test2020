import { PostsService } from './../posts.service';
import { Post } from './../interface/post';
import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import { Observable } from 'rxjs';


@Component({
  selector: 'app-blog',
  templateUrl: './blog.component.html',
  styleUrls: ['./blog.component.css']
})
export class BlogComponent implements OnInit {

  panelOpenState = false;
  userId:string;
  Posts$: Post[];
  userPosts$: Observable<any[]>;


  constructor(private postservice: PostsService,
    private authService:AuthService ) { }

  ngOnInit() {
    this.postservice.getPost()
    .subscribe(data =>this.Posts$ = data );

    this.authService.getUser().subscribe(
      user => {
        this.userId = user.uid;
        this.userPosts$ = this.postservice.getCollection(this.userId);
        console.log(this.userPosts$)
  })}

 public onSubmit(post:Post){
   try{
   this.postservice.savePost(this.userId,post);

  }
  catch(e){ console.log(e)}
}

}
