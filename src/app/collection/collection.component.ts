import { PostsService } from './../posts.service';
import { Post } from './../interface/post';
import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import { Observable } from 'rxjs';


@Component({
  selector: 'app-collection',
  templateUrl: './collection.component.html',
  styleUrls: ['./collection.component.css']
})
export class CollectionComponent implements OnInit {

  panelOpenState = false;
  userId:string;
  userPosts$: Observable<any[]>;
  likes=0;
  constructor(private postservice: PostsService,
    private authService:AuthService ) { }

  ngOnInit() {
    
    this.authService.getUser().subscribe(
      user => {
        this.userId = user.uid; 
        this.userPosts$ = this.postservice.getCollection(this.userId)
  })}
  deletePost(id){
    this.postservice.deletePost(this.userId,id)
    console.log(id);
  }
  addLikes(id,likes){
    this.postservice.addLikes(this.userId,likes,id);
  }
 public onSubmit(post:Post){
   {
  }
}

}
