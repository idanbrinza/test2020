import { Comment } from './../interface/comment';
import { Component, OnInit } from '@angular/core';
import { PostsService } from '../posts.service';
import {ActivatedRoute, Router} from '@angular/router';


@Component({
  selector: 'app-comments',
  templateUrl: './comments.component.html',
  styleUrls: ['./comments.component.css']
})
export class CommentsComponent implements OnInit {
  postId;
 
  panelOpenState = false;

  Comments$: Comment[];

  constructor( private route:ActivatedRoute, private postservice: PostsService) { }

  ngOnInit() {
    this.postservice.getComment()
    .subscribe(data =>this.Comments$ = data ); 
    this.postId = this.route.snapshot.paramMap.get("postid")
  }


}