import { AuthService } from './../auth.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-welcome',
  templateUrl: './welcome.component.html',
  styleUrls: ['./welcome.component.css']
})
export class WelcomeComponent implements OnInit {

  userEmail: String;

  constructor(private authService: AuthService
  ) {
    this.authService.user.subscribe(user => {
      if (user)
        this.userEmail = user.email;
    })
  }


  ngOnInit() {
    console.log(this.userEmail);
  }

}
