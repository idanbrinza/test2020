import { Comment } from './interface/comment';
import { User } from './interface/user';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Post } from './interface/post';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class PostsService {

  postapi = "https://jsonplaceholder.typicode.com/posts "
  userapi = "https://jsonplaceholder.typicode.com/users"
  commentapi = "https://jsonplaceholder.typicode.com/comments"

  userCollection:AngularFirestoreCollection = this.database.collection('users');
  postCollection:AngularFirestoreCollection

  constructor(private _http: HttpClient, private database:AngularFirestore) { }

  getPost(){
    return this._http.get<Post[]>(this.postapi);
  }
  
  getUser(){
    return this._http.get<User[]>(this.userapi);
  }
  getComment(){
    return this._http.get<Comment[]>(this.commentapi);
  }

  getCollection(userId): Observable<any[]> {
    this.postCollection = this.database.collection(`users/${userId}/posts`);
    console.log('Post collection created');
    return this.postCollection.snapshotChanges().pipe(
      map(actions => actions.map(a => {
        const data = a.payload.doc.data();
        data.id = a.payload.doc.id;
        return { ...data };
      }))
    );
  }

  deletePost(userId:string, id:string){
    this.database.doc(`users/${userId}/posts/${id}`).delete();
  }

  addLikes(userId:string, likes:number, id:string){
    this.database.doc(`users/${userId}/posts/${id}`).update({
      likes:likes+1,
    });
  }
  
  savePost(userId:string,post:Post){
    console.log('In add post');
    const postToSave = {id:post.id,title:post.title,body:post.body,likes:0}
    console.log(postToSave);
    this.userCollection.doc(userId).collection('posts').add(postToSave);
  } 
}
